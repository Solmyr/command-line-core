package cz.berescak.clc.commands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Map;

import org.reflections.Reflections;

import com.beust.jcommander.JCommander;

import cz.berescak.clc.specialcommands.ExitCommand;
import cz.berescak.clc.specialcommands.GlobalHelpCommand;
import cz.berescak.clc.specialcommands.HelpCommand;
import cz.berescak.clc.specialcommands.PrintCommand;

public class CommandsManager {
	private Map<String, Class<?>> commandMap;
	private final String HELP_KEYWORD;
	private final String EXIT_KEYWORD;

	public CommandsManager(List<String> commandsPackage, String HELP_KEYWORD, String EXIT_KEYWORD) {
		commandMap = new HashMap<String, Class<?>>();
		
		this.HELP_KEYWORD = HELP_KEYWORD;
		this.EXIT_KEYWORD = EXIT_KEYWORD;
		
		for (String pckg : commandsPackage) {
			Reflections reflections = new Reflections(pckg);
			Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Command.class);
			for (Class<?> clazz : annotated) {
				Command command = clazz.getAnnotation(Command.class);
				commandMap.put(command.name(), clazz);
			}
			
		}	
	}

	public Set<String> getCommandNames() {
		return commandMap.keySet();
	}
	
	public ICommand getCommandInstance(String line) {
		String[] arguments = line.split("\\s+");
		String commandName = arguments[0];
		
		if(commandName.equals(HELP_KEYWORD)) {
			if(arguments.length == 1) {
				GlobalHelpCommand command = new GlobalHelpCommand(commandMap, HELP_KEYWORD, EXIT_KEYWORD);
				return command;
			} else if(arguments.length == 2) {
				HelpCommand command = new HelpCommand(commandMap.get(arguments[1]));
				return command;
			} else {
				return new PrintCommand("Spatne pouziti prikazu " + HELP_KEYWORD);
			}
		}
		
		if(commandName.equals(EXIT_KEYWORD)) {
			return new ExitCommand();
		}
		
		if(commandMap.containsKey(commandName)) {
			Class<?> commandClazz = commandMap.get(commandName);
			try {
				ICommand commandInstance = (ICommand) commandClazz.newInstance();
				arguments = Arrays.copyOfRange(arguments, 1, arguments.length);
				
				new JCommander(commandInstance, arguments);
				return commandInstance;
				
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			return null;
		} else {
			return null;
		}
		
	}
	
	

}
