package cz.berescak.clc.commands;

public interface ICommand {
	public void init();
	public void process();
}
