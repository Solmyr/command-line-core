package cz.berescak.clc;

import java.io.IOException;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.UserInterruptException;
import org.jline.reader.impl.completer.StringsCompleter;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import cz.berescak.clc.commands.CommandsManager;
import cz.berescak.clc.commands.ICommand;
import cz.berescak.clc.specialcommands.ExitCommand;

public class CommandLine {
	private LineReader reader;
	private CommandsManager commandsManager;
	
	private final String PROMPT_SYMBOL;
	private final String UNKNOWN_COMMAND_MESSAGE;
	private final String HELP_KEYWORD;
	private final String EXIT_KEYWORD;

	public static CommandLineBuilder builder() {
		return new CommandLineBuilder();
	}

	public CommandLine(CommandLineConfig clc) {
		PROMPT_SYMBOL = clc.getPromptSymbol();
		UNKNOWN_COMMAND_MESSAGE = clc.getUnknownCommandMessage();
		HELP_KEYWORD = clc.getHelpKeyword();
		EXIT_KEYWORD = clc.getExitKeyword();
		
		try {
			initCommands(clc);
			initLine();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void initCommands(CommandLineConfig clc) {
		commandsManager = new CommandsManager(clc.getCommandsPackage(), HELP_KEYWORD, EXIT_KEYWORD);
	}

	private void initLine() throws IOException {
		Terminal terminal = TerminalBuilder.terminal();
		reader = LineReaderBuilder.builder()
								  .completer(new StringsCompleter(commandsManager.getCommandNames()))
							      .terminal(terminal)
							      .build();
		
	}

	public void run() {
		String line = null;
		try {
			while (true) {
				try {
					line = reader.readLine(PROMPT_SYMBOL);
					ICommand command = commandsManager.getCommandInstance(line);
					if(command == null) {
						System.out.println(UNKNOWN_COMMAND_MESSAGE);
					} else {
						command.process();
					}
					if(command instanceof ExitCommand) {
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		} catch (UserInterruptException e) {
			
		} catch (EndOfFileException e) {
			return;
		}

	}
}
