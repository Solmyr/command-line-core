package cz.berescak.clc.specialcommands;

import cz.berescak.clc.commands.ICommand;

public class PrintCommand implements ICommand{
	private final String MESSAGE;
	
	public PrintCommand(String message) {
		this.MESSAGE = message;
	}

	public void init() {
	}

	public void process() {
		System.out.println(MESSAGE);
	}

}
