package cz.berescak.clc.specialcommands;

import cz.berescak.clc.commands.Command;
import cz.berescak.clc.commands.ICommand;

public class HelpCommand implements ICommand{
	private Class<?> clazz;
	
	public HelpCommand(Class<?> clazz) {
		this.clazz = clazz;
	}

	public void init() {
		
	}

	public void process() {
		if(clazz == null) {
			System.out.println("Neexistujici prikaz");
			return;
		}
		
		Command command = clazz.getAnnotation(Command.class);
		System.out.println(command.name() + " - " + command.description());
		System.out.println("Pouziti: " + command.usage());
		
	}
}
